import json
from osm_bot_abstraction_layer.generic_bot_retagging import run_simple_retagging_task
import learn_from_osm

def main():
    make_bot_edit("Polska")
    make_manual_edit("California")
    make_manual_edit("Texas")

def something_can_be_added(tags_stats, value_dictionary):
    count = tags_stats['natural']['tree']
    for added_key, added_value in value_dictionary.items():
        if added_key not in tags_stats:
            print(added_key, added_value, "has no uses, among", count, "trees")
            return True
        if tags_stats[added_key][added_value] < count:
            print(added_key, added_value, "has", tags_stats[added_key][added_value], "uses, among", count, "trees")
            return True
    return False

def make_bot_edit(area_name):
  is_in_manual_mode = False
  discussion_url = "https://community.openstreetmap.org/t/propozycja-automatycznej-edycji-dodanie-szczegolowych-informacji-na-podstawie-podanego-gatunku-taksonu/109895"
  osm_wiki_documentation_page = "https://wiki.openstreetmap.org/wiki/Mechanical_Edits/Mateusz_Konieczny_-_bot_account/add_detailed_tree_data_based_on_tags_present_already_-_edit_for_Poland"
  data = None
  with open('data.json') as f:
    data = json.load(f)
  for naming_key, entries in data.items():
    common_tags_per_entry = learn_from_osm.build_common_tags_per_entry_dict(naming_key, area_name)
    for naming_value, value_dictionary in entries.items():
        if naming_value not in common_tags_per_entry:
            continue
        tags_stats = common_tags_per_entry[naming_value]
        if something_can_be_added(tags_stats, value_dictionary):
            changeset_comment = naming_key + " = " + naming_value + " - dodawanie dodatkowych informacji które wynikają z tego tagu"
            run_edit_for_specific_entry(naming_key, naming_value, value_dictionary, area_name, is_in_manual_mode, discussion_url, osm_wiki_documentation_page, changeset_comment)

def make_manual_edit(area_name):
  data = None
  with open('data.json') as f:
    data = json.load(f)
  discussion_url = None
  osm_wiki_documentation_page = None
  is_in_manual_mode = True
  for naming_key, entries in data.items():
    common_tags_per_entry = learn_from_osm.build_common_tags_per_entry_dict(naming_key, area_name)
    for naming_value, value_dictionary in entries.items():
        if naming_value not in common_tags_per_entry:
            continue
        tags_stats = common_tags_per_entry[naming_value]
        if something_can_be_added(tags_stats, value_dictionary):
            changeset_comment = naming_key + " = " + naming_value + " - add extra tags based on this info"
            run_edit_for_specific_entry(naming_key, naming_value, value_dictionary, area_name, is_in_manual_mode, discussion_url, osm_wiki_documentation_page, changeset_comment)

def query_for_trees_and_extra_filters(naming_key, naming_value, area_name):
    return """
    [out:xml][timeout:1800];
    area[name=\"""" + area_name + """\"]->.a;
    (
      node[natural=tree][\"""" + naming_key + '"="' + naming_value + """\"](area.a);
    );
    out body;
    >;
    out skel qt;
    """

def edit_element_function_maker(naming_key, naming_value, value_dictionary):
    def edit_element(tags):
        if tags.get(naming_key) != naming_value:
            return tags
        if tags.get("natural") != "tree":
            return tags
        for key, value in value_dictionary.items():
            if key in tags:
                if tags[key] != value:
                    print(naming_key, "=", naming_value, value_dictionary)
                    print(tags)
                    return tags
        for key, value in value_dictionary.items():
            tags[key] = value
        return tags

    return edit_element

def run_edit_for_specific_entry(naming_key, naming_value, value_dictionary, area_name, is_in_manual_mode, discussion_url, osm_wiki_documentation_page, changeset_comment):
    print(naming_key, "=", naming_value)
    run_simple_retagging_task(
        max_count_of_elements_in_one_changeset=500,
        objects_to_consider_query=query_for_trees_and_extra_filters(naming_key, naming_value, area_name),
        cache_folder_filepath='/media/mateusz/OSM_cache/osm_bot_cache',
        is_in_manual_mode=is_in_manual_mode,
        changeset_comment=changeset_comment,
        discussion_url=discussion_url,
        osm_wiki_documentation_page=osm_wiki_documentation_page,
        edit_element_function=edit_element_function_maker(naming_key, naming_value, value_dictionary),
    )

if __name__ == '__main__':
    main()
