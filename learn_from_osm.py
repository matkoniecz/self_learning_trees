# https://github.com/matkoniecz/osm_bot_abstraction_layer?tab=readme-ov-file#downloading-data

from osm_bot_abstraction_layer.overpass_downloader import download_overpass_query
import json
import os.path
import os
import datetime

# https://app.element.io/#/room/#_oftc_#osm:matrix.org
# https://community.openstreetmap.org/t/how-can-i-check-whether-csv-json-overpass-query-failed-or-worked/110597
# How can I distinguish between CSV/JSON overpass query returning no entries and query timeouting and failing? See https://overpass-turbo.eu/s/1mjM for an example of silently failing query.
# At least, silently as far as I can see (looked into browser console and see nothing in response that allows me to distinguish "query timed out" and "all buildings were suddenly deleted")
# Obviously, in this case I can know that building are likely still in OSM - but in many cases such assumptions cannot be made.
# Note: I asked this on IRC on 2024-03-10, I looked at chat 0 hours after asking and there were no replies.

# similar projects
# https://wiki.openstreetmap.org/wiki/Tag:natural%3Dtree/List_of_Genus,_Leaf_cycle,_Leaf_type

def tree_specific_keys():
  return ['wikipedia', 'wikidata', 'denotation', 'fop_id', 'circumference', 'height', 'description', 'note', 'fixme', 'image', 'hiking', 'tourism', 'information', 'start_date', 'planted_date', 'established', 'width', 'diameter', 'protected', 'mapillary', 
  'kodinspire', # TODO! see http://overpass-turbo.eu/s/1IJ3 - 8200 objects https://community.openstreetmap.org/t/kodinspire-vs-ref-inspire/110627
  'planned:natural', # WAT? TODO?
  'inscription', 'historic', 'damaged',
  ]
def tree_specific_wildcards_for_keys():
  return ['ref', 'url', 'website', 'name', 'inscription_date', 'source', 'description', 
  'addr:' # TODO see http://overpass-turbo.eu/s/1IJ4
  ]

def main():
  data = None
  with open('data.json') as f:
    data = json.load(f)
  
  cannot_be_inferred = ['species', 'species:wikidata', 'species:wikipedia' # maybe for given group only more specific values were tagged?
    'natural', # well, cannot be inferred because all have it
  ]
  cannot_be_inferred_wildcard = ['species']
  known_keys_for_harvest = ['leaf_type', 'leaf_cycle']

  for naming_key in ['species:pl', 'species:en', 'genus:pl', 'genus:en', 'taxon:pl', 'taxon:en']: #, 'taxon', 'species']:
    area_name = "Polska"
    common_tags_per_entry = build_common_tags_per_entry_dict(naming_key, area_name)
    for naming_value, tags_stats in common_tags_per_entry.items():
      count = tags_stats['natural']['tree']
      if count == 1:
        continue # just one, not possible to expand at all
      any_infer_chance = False
      for key_in_stats, value_stats in tags_stats.items():
        for proposed_value, value_count in value_stats.items():
          if can_it_be_inferred(key_in_stats, cannot_be_inferred, cannot_be_inferred_wildcard):
            if count != value_count:
              any_infer_chance = True
      if any_infer_chance:
        print()
        print()
        print()
        print()
        print()
        print(naming_key, "=", naming_value, "x" + str(count))
        for key_in_stats, value_stats in tags_stats.items():
          if can_it_be_inferred(key_in_stats, cannot_be_inferred, cannot_be_inferred_wildcard):
            print(key_in_stats, value_stats)
        conflicting_values = False
        harvestable_count = 0
        for key_in_stats, value_stats in tags_stats.items():
          if key_in_stats in known_keys_for_harvest:
            if len(value_stats) != 1:
              conflicting_values = True
            else:
              print(key_in_stats)
              harvestable_count += 1
        if conflicting_values == False and harvestable_count >= 2:
          for key_in_stats, value_stats in tags_stats.items():
            if key_in_stats in known_keys_for_harvest:
              if naming_key not in data:
                data[naming_key] = {}
              if naming_value not in data[naming_key]:
                data[naming_key][naming_value] = {}
              if key_in_stats not in data[naming_key][naming_value]:
                data[naming_key][naming_value][key_in_stats] = list(value_stats)[0]

  with open("data.json", "w") as output:
      # magic happens here to make it pretty-printed
      output.write(
          json.dumps(data, indent=4, sort_keys=True, ensure_ascii=False)
      )              

def can_it_be_inferred(key_in_stats, cannot_be_inferred, cannot_be_inferred_wildcard):
  if key_in_stats in cannot_be_inferred:
    return False
  for wildcard in cannot_be_inferred_wildcard:
    if wildcard in key_in_stats:
      return False
  return True

def build_common_tags_per_entry_dict(naming_key, area_name):
  common_tags_per_entry = {}
  for entry in get_raw_osm_data(naming_key, area_name):
    indexing_name = entry['tags'][naming_key]
    for key, value in entry['tags'].items():
      skip = False
      for wildcard in tree_specific_wildcards_for_keys():
        if wildcard in key:
          skip = True
          break
      if skip:
        continue
      if key in tree_specific_keys():
        continue
      #print(key, value)
      if indexing_name not in common_tags_per_entry:
        common_tags_per_entry[indexing_name] = {}
      if key not in common_tags_per_entry[indexing_name]:
        common_tags_per_entry[indexing_name][key] = {}
      if value not in common_tags_per_entry[indexing_name][key]:
        common_tags_per_entry[indexing_name][key][value] = 0
      common_tags_per_entry[indexing_name][key][value] += 1
  return common_tags_per_entry

def get_raw_osm_data(key, area_name):
  now = datetime.datetime.now()
  day_code = str(now.year) + "-" + str(now.month) + "-day_based_but_not_day-" + str(now.day)
  storage_path = '/media/mateusz/OSM_cache/osm_bot_cache/trees_with_' + key + '_in_' + area_name + day_code + '.json'
  if os.path.isfile(storage_path) == False or os.stat(storage_path).st_size == 0:
    query="""
    [out:json][timeout:1800];
    area[name=\"""" + area_name + """\"]->.a;
    (
      node[natural=tree][\"""" + key + """\"](area.a);
    );
    out body;
    >;
    out skel qt;
    """
    download_overpass_query(query, storage_path)
  else:
    print("reusing", storage_path)
  with open(storage_path) as file:
      data = json.load(file)
      for entry in data['elements']:
        yield entry

if __name__ == '__main__':
  main()